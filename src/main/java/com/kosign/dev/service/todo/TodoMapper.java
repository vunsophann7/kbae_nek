package com.kosign.dev.service.todo;

import com.kosign.dev.domain.todo.Todo;
import com.kosign.dev.domain.todo.TodoRepository;
import com.kosign.dev.payload.todo.TodoRequest;
import com.kosign.dev.payload.todo.TodoResponse;
import org.springframework.stereotype.Component;

@Component
public class TodoMapper {

    private final TodoRepository todoRepository;

    public TodoMapper(TodoRepository todoRepository) {
        this.todoRepository = todoRepository;
    }

    public Todo mapToTodoEntity(TodoRequest payload) {



        return Todo.builder()
                .title(payload.title())
                .build();
    }

    public TodoResponse mapToTodoResponse(Todo todo) {

        return TodoResponse.builder()
                .title(todo.getTitle())
                .build();
    }
}
