package com.kosign.dev.controller;

import com.kosign.dev.payload.todo.TodoRequest;
import com.kosign.dev.properties.JwtProperties;
import com.kosign.dev.service.todo.TodoService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/todo")
@RequiredArgsConstructor
public class TodoController extends AbstractRestController{
    private final TodoService todoService;
    private final JwtProperties jwtProperties;

    @PostMapping
    public Object createTodo(@RequestBody TodoRequest payload) {
        todoService.createTodo(payload);
        return ok();
    }

    @GetMapping
    public Object getTodoList() {
        return ok(todoService.getTodoList());
    }

    @GetMapping("/{id}")
    public Object getById(@PathVariable("id") Long id) {
        return ok(todoService.getById(id));
    }
}
